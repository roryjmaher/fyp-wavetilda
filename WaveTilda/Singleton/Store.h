//
//  Store.h
//  WaveTilda
//
//  Created by Rory Maher on 30/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Store : NSObject
{
    //Local Vars here.....
    
    BOOL answerOne;
    BOOL answerTwo;
    BOOL answerThree;
    BOOL answerFour;
    BOOL answerFive;
    BOOL answerSix;
    BOOL answerSeven;
    BOOL answerEight;
    
    int numberOfCorrectAnswers;
    
    NSString *trophyEarned;
    
    BOOL objectivesOnce;
    BOOL helpOnce;
    BOOL quizHelpOnce;
    
    NSString *feedBackOneBool;
    
}

//Properties here.....
@property (nonatomic, assign) BOOL answerOne;
@property (nonatomic, assign) BOOL answerTwo;
@property (nonatomic, assign) BOOL answerThree;
@property (nonatomic, assign) BOOL answerFour;
@property (nonatomic, assign) BOOL answerFive;
@property (nonatomic, assign) BOOL answerSix;
@property (nonatomic, assign) BOOL answerSeven;
@property (nonatomic, assign) BOOL answerEight;


@property (nonatomic, assign) int numberOfCorrectAnswers;
@property (nonatomic, copy) NSString *trophyEarned;

@property (nonatomic, assign)BOOL objectivesOnce;
@property (nonatomic, assign) BOOL helpOnce;
@property (nonatomic, assign) BOOL quizHelpOnce;

@property (nonatomic, copy) NSString *feedBackOneBool;
@property (nonatomic, copy) NSString *feedBackTwoBool;
@property (nonatomic, copy) NSString *feedBackThreeBool;
@property (nonatomic, copy) NSString *feedBackFourBool;


//Public Singleton Class

+ (Store *) sharedStore;

@end
