//
//  Store.m
//  WaveTilda
//
//  Created by Rory Maher on 30/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "Store.h"

@implementation Store
@synthesize answerOne,answerTwo,answerThree,answerFour,answerFive,answerSix,answerSeven,answerEight,trophyEarned,objectivesOnce,feedBackOneBool,feedBackTwoBool,feedBackThreeBool,feedBackFourBool,helpOnce,quizHelpOnce,numberOfCorrectAnswers;


static Store *sharedStore = nil;

+ (Store *) sharedStore
{
    if (sharedStore == nil)
    {
        sharedStore = [[self alloc] init];
    }
    return sharedStore;
}

@end
