//
//  SoundFundFive.h
//  WaveTilda
//
//  Created by Rory Maher on 19/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoundFundFive : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *wavelengthImage;

- (IBAction)popToFour:(UIButton *)sender;
- (IBAction)toMenuFive:(UIButton *)sender;

@property (nonatomic, assign) NSUInteger wavelengthCounter;
@property (weak, nonatomic) IBOutlet UITextView *wavelengthText;
@property (weak, nonatomic) IBOutlet UITextView *waveLengthTextTwo;

- (IBAction)wavelengthTap:(UITapGestureRecognizer *)sender;

@property (weak, nonatomic) IBOutlet UIProgressView *wavelengthProgress;



@end
