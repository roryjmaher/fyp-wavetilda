//
//  WaveTildaAppDelegate.m
//  WaveTilda
//
//  Created by Rory Maher on 05/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "WaveTildaAppDelegate.h"
#import "Store.h"

@interface WaveTildaAppDelegate()

@property (strong,nonatomic,readonly) PdAudioController *audioController;
-(void)setupPd;

@end



@implementation WaveTildaAppDelegate
{
    Store *setObjective;
}

@synthesize audioController = _audioController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [self setupPd];
    
    //UIAlertView *headphoneAlert = [[UIAlertView alloc] initWithTitle:@"Volume Warning!" message:@"Please keep the Volume of the App at a responsible level." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        //[headphoneAlert show];
    
    
    setObjective = [Store sharedStore];
    setObjective.objectivesOnce = TRUE;
    return YES;
}

-(void)setupPd
{
    
    //Initialising libPd for the application
    _audioController = [[PdAudioController alloc] init];
    
    if ([self.audioController configureAmbientWithSampleRate:44100 numberChannels:2 mixingEnabled:YES] != PdAudioOK)
    {
        NSLog(@"Failed to Initialiase Audio Components!");
    }
    
    dispatcher = [[PdDispatcher alloc] init];
    [PdBase setDelegate:dispatcher];
    audioController.active = YES;
   
    
    // log actually settings
	[self.audioController print];
    
	// set AppDelegate as PdRecieverDelegate to recieve messages from pd
    [PdBase setDelegate:self];
    
	// recieve all [send load-meter] messages from pd
    [PdBase subscribe:@"rippleBang"];
    [PdBase subscribe:@"startSpeaker"];
    [PdBase subscribe:@"stopSpeaker"];
    [PdBase subscribe:@"freqReceive"];
    [PdBase subscribe:@"alarmBang"];
    [PdBase subscribe:@"talkingBang"];
    [PdBase subscribe:@"talkingStop"];
    [PdBase subscribe:@"radioBang"];
    [PdBase subscribe:@"alarmStop"];
    [PdBase subscribe:@"radioStop"];
    [PdBase subscribe:@"woodBang"];
    [PdBase subscribe:@"metalBang"];
    [PdBase subscribe:@"waterBang"];
    [PdBase subscribe:@"freqSweepUp"];
    [PdBase subscribe:@"freqSweepDown"];
    [PdBase subscribe:@"sweepUpVol"];
    [PdBase subscribe:@"sweepDownVol"];
    [PdBase subscribe:@"setTop"];
    [PdBase subscribe:@"setBottom"];
    [PdBase subscribe:@"phaseStart"];
    [PdBase subscribe:@"phaseStop"];
    [PdBase subscribe:@"phaseVolDown"];
    [PdBase subscribe:@"phaseVolUp"];
    [PdBase subscribe:@"acousticBang"];
    [PdBase subscribe:@"bassBang"];
    [PdBase subscribe:@"pianoBang"];
    [PdBase subscribe:@"frenchHornBang"];
    [PdBase subscribe:@"timbreVolUp"];
    [PdBase subscribe:@"timbreVolDown"];
    //[PdBase subscribe:@""];


}


- (void)applicationWillResignActive:(UIApplication *)application
{
    
    self.audioController.active  = NO;

    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    self.audioController.active  = YES;
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
