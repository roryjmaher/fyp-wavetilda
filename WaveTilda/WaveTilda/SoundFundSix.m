//
//  SoundFundSix.m
//  WaveTilda
//
//  Created by Rory Maher on 20/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "SoundFundSix.h"


@interface SoundFundSix ()
{
    
}

@end

@implementation SoundFundSix


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.amplitudeCounter = 0;
    self.ampView.amplitude = 30;
    self.amplitudeText.text = @"Amplitude refers to the 'loudness' of a sound. This is affected by the intensity of the source objects vibration.";
    self.amplitudeTextTwo.text = @"The higher the amplitude the 'louder' the sound. The lower the amplitude the 'quieter' the sound.";

	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.amplitudeCounter = 0;
    self.ampView.amplitude = 30;
    self.amplitudeText.text = @"Amplitude refers to the 'loudness' of a sound. This is the intensity of the sources vibration.";
    self.amplitudeTextTwo.text = @"The higher the amplitude the 'louder' the sound. The lower the amplitude the 'quieter' the sound.";
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ampSlider:(UISlider *)sender {
    
    self.ampView.amplitude = sender.value;
}

- (IBAction)popToFive:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)toMenuSix:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pinchAmplitude:(UIPinchGestureRecognizer *)sender
{
    
    float scale = [(UIPinchGestureRecognizer*)sender scale];
    
    scale = scale * 20;
    
    if (scale > 120) {
        scale = 120;
    }
    
    self.ampView.amplitude = scale;

    
}

- (IBAction)amplitudeTap:(UITapGestureRecognizer *)sender {
    
    self.amplitudeCounter++;
    
    if (self.amplitudeCounter > 2) {
        self.amplitudeCounter = 0;
    }
    
    if (self.amplitudeCounter == 0) {
        
        self.amplitudeImages.image = [UIImage imageNamed:@"Amplitude_001.png"];
        self.amplitudeProgress.progress = 0.33;
        self.amplitudeText.text = @"Amplitude refers to the 'loudness' of a sound. This is affected by the intensity of the source objects vibration.";
        self.amplitudeTextTwo.text = @"The higher the amplitude the 'louder' the sound. The lower the amplitude the 'quieter' the sound.";
        
    }
    else if (self.amplitudeCounter == 1)
    {
        self.amplitudeImages.image = [UIImage imageNamed:@"Amplitude_002.png"];
        self.amplitudeProgress.progress = 0.66;
        self.amplitudeText.text = @"The waveform contains 'hills' and 'valleys' or Peaks and Troughs.";
        self.amplitudeTextTwo.text = @"The Peak is the highest point of the waveform whereas the Trough is the lowest. Higher Peaks & Troughs mean a 'louder' sound.";
    }
    else if (self.amplitudeCounter == 2)
    {
        self.amplitudeImages.image = [UIImage imageNamed:@"Amplitude_003.jpg"];
        self.amplitudeProgress.progress = 1;
        self.amplitudeText.text = @"Amplitude is measured in Decibels (dB). This scale is standard in music and audio production.";
        self.amplitudeTextTwo.text = @"Peak amplitude measures from zero to the Peak of the waveform. Peak-to-Peak measures from Trough to Peak.";
    }
    
}

@end

