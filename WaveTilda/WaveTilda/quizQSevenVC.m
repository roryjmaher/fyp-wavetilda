//
//  quizQSevenVC.m
//  WaveTilda
//
//  Created by Rory Maher on 31/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "quizQSevenVC.h"

@interface quizQSevenVC ()
{
    Store *qSevenStore;
}

@end

@implementation quizQSevenVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    qSevenStore = [Store sharedStore];
    
    self.ansOneCount = 0;
    self.ansTwoCount = 0;
    self.ansThreeCount = 0;
    self.ansFourCount = 0;
    
    self.checkAnsSeven.enabled = NO;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:37.0/255.0 green:53.0/255.0 blue:68.0/255.0 alpha:1.0]];
    self.toQEight.enabled = NO;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ansOne:(UIButton *)sender {
    
    self.ansOneCount++;
    
    if (self.ansOneCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsSeven.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansTwoAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsSeven.enabled = YES;
    }

    
}

- (IBAction)ansTwo:(UIButton *)sender {
    
    self.ansTwoCount++;
    
    if (self.ansTwoCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsSeven.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsSeven.enabled = YES;
    }

}

- (IBAction)ansThree:(UIButton *)sender {
    
    self.ansThreeCount++;
    
    if (self.ansThreeCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsSeven.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansTwoAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansFourCount = 0;
        self.checkAnsSeven.enabled = YES;
    }

}

- (IBAction)ansFour:(UIButton *)sender {
    
    self.ansFourCount++;
    
    if (self.ansFourCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsSeven.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansTwoAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.checkAnsSeven.enabled = YES;
    }
}


- (IBAction)qSevenResult:(UIButton *)sender {
    
    if (self.ansFourCount % 2 != 0)
    {
        
        [self.view setBackgroundColor:[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:0.7]];
        self.qSevenResultLabel.text = @"Correct! - Proceed to the Next Question.";
        self.ansOneAtt.enabled = NO;
        self.ansTwoAtt.enabled = NO;
        self.ansThreeAtt.enabled = NO;
        self.ansFourAtt.enabled = NO;
        self.toQEight.enabled = YES;
        qSevenStore.numberOfCorrectAnswers++;
        qSevenStore.answerSeven  = TRUE;
    }
    
    else
    {
        [self.view setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7]];
        self.qSevenResultLabel.text = @"Incorrect! - Proceed to the Next Question.";
        self.ansOneAtt.enabled = NO;
        self.ansTwoAtt.enabled = NO;
        self.ansThreeAtt.enabled = NO;
        self.ansFourAtt.enabled = NO;
        self.toQEight.enabled = YES;
        qSevenStore.answerSeven  = NO;
    }

}
@end
