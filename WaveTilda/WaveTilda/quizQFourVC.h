//
//  quizQFourVC.h
//  WaveTilda
//
//  Created by Rory Maher on 31/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"

@interface quizQFourVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *toQFiveButton;

@property (weak, nonatomic) IBOutlet UILabel *qFourAnsLabel;


@property (weak, nonatomic) IBOutlet UIButton *checkAnsFour;


- (IBAction)ansBtnOne:(UIButton *)sender;
- (IBAction)ansBtnTwo:(UIButton *)sender;
- (IBAction)ansBtnThree:(UIButton *)sender;
- (IBAction)ansBtnFour:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *ansAttOne;
@property (weak, nonatomic) IBOutlet UIButton *ansAttTwo;
@property (weak, nonatomic) IBOutlet UIButton *ansAttThree;
@property (weak, nonatomic) IBOutlet UIButton *ansAttFour;


@property (nonatomic, assign) NSUInteger ansOneCount;
@property (nonatomic, assign) NSUInteger ansTwoCount;
@property (nonatomic, assign) NSUInteger ansThreeCount;
@property (nonatomic, assign) NSUInteger ansFourCount;


- (IBAction)questionFourResult:(UIButton *)sender;





@end
