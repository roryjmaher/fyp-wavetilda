//
//  chooseQuizVC.h
//  WaveTilda
//
//  Created by Rory Maher on 29/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface chooseQuizVC : UIViewController
- (IBAction)chooseQuizToMenu:(UIButton *)sender;

- (IBAction)backToTimbre:(UIButton *)sender;
- (IBAction)toQuiz:(UIButton *)sender;
@end
