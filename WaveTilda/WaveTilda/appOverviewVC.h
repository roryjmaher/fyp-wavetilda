//
//  appOverviewVC.h
//  WaveTilda
//
//  Created by Rory Maher on 02/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface appOverviewVC : UIViewController

- (IBAction)dismissObjective:(UIButton *)sender;

@end
