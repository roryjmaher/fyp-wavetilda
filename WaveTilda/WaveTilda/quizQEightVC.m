//
//  quizQEightVC.m
//  WaveTilda
//
//  Created by Rory Maher on 31/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "quizQEightVC.h"

@interface quizQEightVC ()
{
    Store *qEightStore;
}

@end

@implementation quizQEightVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    qEightStore = [Store sharedStore];
    
    [super viewDidLoad];
    
    self.ansOneCount = 0;
    self.ansTwoCount = 0;
    self.ansThreeCount = 0;
    self.ansFourCount = 0;
    
    self.checkAnsEight.enabled = NO;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:37.0/255.0 green:53.0/255.0 blue:68.0/255.0 alpha:1.0]];
    self.finishQuiz.enabled = NO;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)ansOne:(UIButton *)sender {
    
    self.ansOneCount++;
    
    if (self.ansOneCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsEight.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansTwoAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsEight.enabled = YES;
    }

    
}

- (IBAction)ansTwo:(UIButton *)sender {
    
    self.ansTwoCount++;
    
    if (self.ansTwoCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsEight.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsEight.enabled = YES;
    }
}

- (IBAction)ansThree:(UIButton *)sender {
    
    self.ansThreeCount++;
    
    if (self.ansThreeCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsEight.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansTwoAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansFourCount = 0;
        self.checkAnsEight.enabled = YES;
    }
}

- (IBAction)ansFour:(UIButton *)sender {
    
    self.ansFourCount++;
    
    if (self.ansFourCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsEight.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansTwoAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeAtt setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.checkAnsEight.enabled = YES;
    }
}

- (IBAction)quizEightResult:(UIButton *)sender {
    
    if (self.ansOneCount % 2 != 0)
    {
        
        [self.view setBackgroundColor:[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:0.7]];
        self.quizEightLabel.text = @"Correct! - Proceed to the Next Screen.";
        self.ansOneAtt.enabled = NO;
        self.ansTwoAtt.enabled = NO;
        self.ansThreeAtt.enabled = NO;
        self.ansFourAtt.enabled = NO;
        self.finishQuiz.enabled = YES;
        qEightStore.numberOfCorrectAnswers++;
        qEightStore.answerEight = TRUE;
    }
    
    else
    {
        [self.view setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7]];
        self.quizEightLabel.text = @"Incorrect! - Proceed to the Next Screen.";
        self.ansOneAtt.enabled = NO;
        self.ansTwoAtt.enabled = NO;
        self.ansThreeAtt.enabled = NO;
        self.ansFourAtt.enabled = NO;
        self.finishQuiz.enabled = YES;
        qEightStore.answerEight = NO;
    }
}

@end
