//
//  SoundFundNine.m
//  WaveTilda
//
//  Created by Rory Maher on 27/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "SoundFundNine.h"
#import "PdBase.h"
#import "PdDispatcher.h"
#import "WaveTildaAppDelegate.h"

@interface SoundFundNine ()

@end

@implementation SoundFundNine

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    patch = [PdBase openFile:@"timbre.pd" path:[[NSBundle mainBundle] bundlePath]];
    
    self.timbreCounter = 1;
    self.timbreImage.image = [UIImage imageNamed:@"acousticGuitar.jpg"];
    self.timbreLabel.text = @"Tap Image to Hear Instruments";
    self.timbreTextOne.text = @"Now that we have learned about frequency, we can discuss the characteristics of a sound. When we play the same frequency across multiple instruments - why does that same frequency sound different?";
    self.timbreTextTwo.text = @"Each instrument sounds uniquely different.This difference in tonal quality is called Timbre (pronounced tam-ber). This is because most real world sounds contain many different frequencies at different amplitudes. This is what gives them their unique tonal characteristics.";
    self.timbreProgress.progress = 0.25;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    self.timbreCounter = 1;
    self.timbreImage.image = [UIImage imageNamed:@"acousticGuitar.jpg"];
    self.timbreLabel.text = @"Tap Image to Hear Instruments";
    self.timbreTextOne.text = @"Tap Image to Hear Instruments";
    self.timbreTextOne.text = @"Now that we have learned about frequency, we can discuss the characteristics of a sound. When we play the same frequency across multiple instruments - why does that same frequency sound different?";
    self.timbreTextTwo.text = @"Each instrument sounds uniquely different.This difference in tonal quality is called Timbre (pronounced tam-ber). This is because most real world sounds contain many different frequencies at different amplitudes. This is what gives them their unique tonal characteristics.";
    self.timbreProgress.progress = 0.25;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)popToEight:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menuFromNine:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
 [PdBase subscribe:@"acousticBang"];
 [PdBase subscribe:@"bassBang"];
 [PdBase subscribe:@"pianoBang"];
 [PdBase subscribe:@"frenchHornBang"];
 [PdBase subscribe:@"timbreVolUp"];
 [PdBase subscribe:@"timbreVolDown"];
 */


- (IBAction)timbreTap:(UITapGestureRecognizer *)sender
{
    self.timbreCounter++;
    
    if (self.timbreCounter > 4) {
        self.timbreCounter = 1;
    }
    
    
    if (self.timbreCounter == 1)
    {
        self.timbreImage.image = [UIImage imageNamed:@"acousticGuitar.jpg"];
        [PdBase sendBangToReceiver:@"acousticBang"];
        self.timbreLabel.text = @"Acoustic Guitar playing 440Hz";
        self.timbreTextTwo.text = @"Each instrument sounds uniquely different.This difference in tonal quality is called Timbre (pronounced tam-ber). This is because most real world sounds contain many different frequencies at different amplitudes and this is what gives them their unique tonal characteristics.";
        self.timbreProgress.progress = 0.25;
        
    }
    else if (self.timbreCounter == 2)
    {
        self.timbreImage.image = [UIImage imageNamed:@"bassGuitar.jpg"];
        [PdBase sendBangToReceiver:@"bassBang"];
        self.timbreLabel.text = @"Bass Guitar playing 440Hz";
        self.timbreTextTwo.text = @"The Sine Wave is the most basic of all sound. It is simply a piece of audio with just one frequency present. This type of frequency does not naturally occur in the real world. Most audio we hear in our day to day lives has many frequencies. We call these pieces of audio Complex Waveforms.";
        self.timbreProgress.progress = 0.50;
        
    }
    else if (self.timbreCounter == 3)
    {
        self.timbreImage.image = [UIImage imageNamed:@"grandPiano.jpg"];
        [PdBase sendBangToReceiver:@"pianoBang"];
        self.timbreLabel.text = @"Grand Piano playing 440Hz";
        self.timbreTextTwo.text = @"Complex waveforms are merely just a combination of multiple simple waveforms vibrating at the same time. As we have discussed, the sine wave is the most basic of all sound. When we start adding many sine waves, at many different frequencies, we can achieve complex waveforms. Complex waveforms contain many simple frequencies. These frequencies are named Partials.";
        self.timbreProgress.progress = 0.75;
        
    }
    else if (self.timbreCounter == 4)
    {
        self.timbreImage.image = [UIImage imageNamed:@"frenchHorn.png"];
        [PdBase sendBangToReceiver:@"frenchHornBang"];
        self.timbreLabel.text = @"French Horn playing 440Hz";
        self.timbreTextTwo.text = @"The lowest frequency or Partial is called The Fundamental Frequnecy. This is used to name the note. If the audio is harmonic then all of the partials will be Whole number multiples of the fundamental frequency (e.g. 440 x 2, 440 x 3 etc...). If the audio is inharmonic then the partials are not strictly whole number multiples and could include numbers such as (440 * 2.4 and 440 * 4.7 etc.).";
        self.timbreProgress.progress = 1;
        
    }
    
    
}

@end
