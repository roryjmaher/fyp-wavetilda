//
//  SoundFundFive.m
//  WaveTilda
//
//  Created by Rory Maher on 19/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "SoundFundFive.h"

@interface SoundFundFive ()

@end

@implementation SoundFundFive

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.wavelengthCounter = 0;
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.wavelengthImage.image = [UIImage imageNamed:@"wavelength.png"];
    self.wavelengthText.text = @"The first characteristic we will discuss is the wavelength of a waveform.";
    self.waveLengthTextTwo.text = @"Wavelength refers to the distance from one identical point of the waveform to the next identical point.";
    self.wavelengthCounter = 0;
}

-(void)viewDidDisappear:(BOOL)animated
{

}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)popToFour:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)toMenuFive:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)wavelengthTap:(UITapGestureRecognizer *)sender
{
    
    self.wavelengthCounter++;
    
    if (self.wavelengthCounter > 4) {
        self.wavelengthCounter = 0;
    }
    
    if (self.wavelengthCounter == 0)
    {
        self.wavelengthImage.image = [UIImage imageNamed:@"wavelength.png"];
        self.wavelengthText.text = @"The first characteristic we will discuss is the wavelength of a waveform.";
        self.waveLengthTextTwo.text = @"Wavelength refers to the distance from one identical point of the waveform to the next identical point.";
        self.wavelengthProgress.progress = 0.2;
        
    }
    else if (self.wavelengthCounter == 1)
    {
        self.wavelengthImage.image = [UIImage imageNamed:@"WavelengthThree.gif"];
        self.wavelengthText.text = @"Wavelength is usually measured in meters per second.";
        self.waveLengthTextTwo.text = @"Wavelength is normally written with the Greek letter lambda (λ).";
                self.wavelengthProgress.progress = 0.4;
    }
    else if (self.wavelengthCounter == 2)
    {
        self.wavelengthImage.image = [UIImage imageNamed:@"wavelengthOne.gif"];
        self.wavelengthText.text = @"Wavelength is usually plotted on the X-Axis.";
        self.waveLengthTextTwo.text = @"Whereas Amplitude is usually plotted on the Y-Axis.";
                self.wavelengthProgress.progress = 0.6;
    }
    else if (self.wavelengthCounter == 3)
    {
        self.wavelengthImage.image = [UIImage imageNamed:@"wavelengthTwo.gif"];
        self.wavelengthText.text = @"Wavelength is closely related to the frequency of a Waveform.";
        self.waveLengthTextTwo.text = @"The lower the frequency, the longer the Wavelength. Also, the higher the Frequency the shorter the Wavelength.";
                self.wavelengthProgress.progress = 0.8;
    }
    else if (self.wavelengthCounter == 4)
    {
        self.wavelengthImage.image = [UIImage imageNamed:@"wavelengths.gif"];
        self.wavelengthText.text = @"Wavelength can be measured from any point of the Waveform.";
        self.waveLengthTextTwo.text = @"This can be from the Peak or the Trough of the wave where the wave crosses or intersects the X-Axis.";
                self.wavelengthProgress.progress = 1;
    }
}


@end
