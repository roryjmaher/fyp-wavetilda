//
//  soundFundamentalsOne.m
//  WaveTilda
//
//  Created by Rory Maher on 05/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "soundFundamentalsOne.h"
#import "Store.h"

@interface soundFundamentalsOne ()

@end


@implementation soundFundamentalsOne
{
    NSMutableArray *alarmArray;
    NSMutableArray *talkingArray;
    NSMutableArray *arnoldArray;
    Store *helpBool;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    helpBool = [Store sharedStore];
    patch = [PdBase openFile:@"gifAudio.pd" path:[[NSBundle mainBundle] bundlePath]];
    
    [super viewDidLoad];
    
    //Alarm Clock Array
    alarmArray = [NSMutableArray array];
    talkingArray = [NSMutableArray array];
    arnoldArray = [NSMutableArray array];
    
    
    //Alarm Gif Loading
    for (NSUInteger i =1; i < 10; i++)
    {
        NSString *alarmNames = [NSString stringWithFormat:@"alarmClock_%lu.gif", (unsigned long)i];
        [alarmArray addObject:[UIImage imageNamed:alarmNames]];
    }
    self.alarmClockView.animationImages = alarmArray;
    self.alarmCounter = 0;

    
    //Talking Gif Loading
    for (NSUInteger j=1; j<17; j++)
    {
        NSString *talkingNames = [NSString stringWithFormat:@"talkingPeople_%lu.gif", (unsigned long)j];
        [talkingArray addObject:[UIImage imageNamed:talkingNames]];
    }
    self.talkingView.animationImages = talkingArray;
    self.talkingCounter = 0;
    
    
    //Arnold GIf Loading
    for (NSUInteger k = 1; k < 7; k++)
    {
        NSString *arnoldNames = [NSString stringWithFormat:@"arnoldListening_%lu.gif", (unsigned long)k];
        [arnoldArray addObject:[UIImage imageNamed:arnoldNames]];
    }
    self.arnoldView.animationImages = arnoldArray;
    self.arnoldCounter = 0;
    
    }

-(void)viewDidAppear:(BOOL)animated
{
    [self.alarmClockView stopAnimating];
    [self.talkingView stopAnimating];
    [self.arnoldView stopAnimating];
    self.alarmCounter = 0;
    self.arnoldCounter = 0;
    self.talkingCounter = 0;
    
    if (helpBool.helpOnce == FALSE) {
        [self performSegueWithIdentifier:@"launchHelp" sender:self];
    }
    else
    {
        helpBool.helpOnce = TRUE;
    }
    helpBool.helpOnce = TRUE;
}


//For me if I comment out[PdBase closeFile:patch]; I have no issues.
-(void)viewDidDisappear:(BOOL)animated
{
    [PdBase setDelegate:nil];
    //[PdBase closeFile:patch];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.alarmClockView stopAnimating];
    [self.talkingView stopAnimating];
    [self.arnoldView stopAnimating];
    [PdBase sendBangToReceiver:@"alarmStop"];
    [PdBase sendBangToReceiver:@"talkingStop"];
    [PdBase sendBangToReceiver:@"radioStop"];

}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (BOOL)prefersStatusBarHidden
{
    return YES;
}



//Using the tap gesture recognizer to start/stop the gif (Uses Modulo)
- (IBAction)alarmStartStop:(UITapGestureRecognizer *)sender {
    
    [self.alarmClockView setAnimationDuration:1.0f];
    self.alarmCounter++;
    
    if (self.alarmCounter % 2 == 0) {
        [self.alarmClockView stopAnimating];
        [PdBase sendBangToReceiver:@"alarmStop"];
    }
    else
    {
        [self.alarmClockView startAnimating];
        [PdBase sendBangToReceiver:@"alarmBang"];
        
        [PdBase sendBangToReceiver:@"radioStop"];
        [PdBase sendBangToReceiver:@"talkingStop"];
        
        [self.talkingView stopAnimating];
        [self.arnoldView stopAnimating];
        
        self.talkingCounter = 0;
        self.arnoldCounter = 0;
    }
    
}



- (IBAction)talkingStartStop:(UITapGestureRecognizer *)sender {
    
    [self.talkingView setAnimationDuration:1.5f];
    self.talkingCounter++;
    
    if (self.talkingCounter % 2 == 0) {
        [self.talkingView stopAnimating];
        [PdBase sendBangToReceiver:@"talkingStop"];
    }
    else
    {
        [self.talkingView startAnimating];
        [PdBase sendBangToReceiver:@"talkingBang"];
        [self.alarmClockView stopAnimating];
        [self.arnoldView stopAnimating];
        
        [PdBase sendBangToReceiver:@"alarmStop"];
        [PdBase sendBangToReceiver:@"radioStop"];
        
        self.alarmCounter = 0;
        self.arnoldCounter = 0;
        
    }
    
}



- (IBAction)arnoldStartStop:(UITapGestureRecognizer *)sender {
    

    [self.arnoldView setAnimationDuration:0.5f];
    self.arnoldCounter++;
    
    if (self.arnoldCounter % 2 == 0)
    {
        [self.arnoldView stopAnimating];
        [PdBase sendBangToReceiver:@"radioStop"];
    }
    
    else
    {
        [self.arnoldView startAnimating];
        [PdBase sendBangToReceiver:@"radioBang"];
        [self.alarmClockView stopAnimating];
        [self.talkingView stopAnimating];
        
        [PdBase sendBangToReceiver:@"alarmStop"];
        [PdBase sendBangToReceiver:@"talkingStop"];
        
        self.alarmCounter = 0;
        self.talkingCounter = 0;
    }
    
}

- (IBAction)menuOne:(UIButton *)sender
    {
        
    [self dismissViewControllerAnimated:YES completion:nil];
        
    }


@end
