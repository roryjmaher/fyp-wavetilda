//
//  SoundFundFour.h
//  WaveTilda
//
//  Created by Rory Maher on 17/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SoundFundFour : UIViewController
{
    //I-vars
    int frequency;
}
- (IBAction)soundFourMenuBtn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIImageView *waveformImageView;
@property (weak, nonatomic) IBOutlet UIImageView *waveLengthView;
@property (weak, nonatomic) IBOutlet UIImageView *amplitudeView;
@property (weak, nonatomic) IBOutlet UIImageView *frequencyView;

@property (weak, nonatomic) IBOutlet UILabel *wavelengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *amplitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *frequencyLabel;

@property (nonatomic, assign) NSUInteger sineCounter;
@property (nonatomic, assign) NSUInteger wavelengthCounter;
@property (nonatomic, assign) NSUInteger amplitudeCounter;
@property (nonatomic, assign) NSUInteger frequencyCounter;


- (IBAction)popToThree:(UIButton *)sender;
- (IBAction)sineStartStop:(UITapGestureRecognizer *)sender;

- (IBAction)wavelengthTap:(UITapGestureRecognizer *)sender;
- (IBAction)amplitudeTap:(UITapGestureRecognizer *)sender;
- (IBAction)frequencyTap:(UITapGestureRecognizer *)sender;



@end
