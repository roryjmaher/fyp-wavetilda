//
//  quizQThreeVC.m
//  WaveTilda
//
//  Created by Rory Maher on 31/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "quizQThreeVC.h"

@interface quizQThreeVC ()
{
    Store *qThreeStore;
}

@end

@implementation quizQThreeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    qThreeStore = [Store sharedStore];
    self.ansOneCount = 0;
    self.ansTwoCount = 0;
    self.ansThreeCount = 0;
    self.ansFourCount = 0;
    
    self.checkAnsThree.enabled = NO;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:37.0/255.0 green:53.0/255.0 blue:68.0/255.0 alpha:1.0]];
    self.toQFour.enabled = NO;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)qThreeAnsOne:(UIButton *)sender {
    
    self.ansOneCount++;
    
    if (self.ansOneCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsThree.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.answerTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsThree.enabled = YES;
    }
}

- (IBAction)qThreeAnsTwo:(UIButton *)sender {
    
    self.ansTwoCount++;
    
    if (self.ansTwoCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsThree.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.answerOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsThree.enabled = YES;
    }

    
}

- (IBAction)qThreeAnsThree:(UIButton *)sender {
    
    self.ansThreeCount++;
    
    if (self.ansThreeCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsThree.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.answerOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansFourCount = 0;
        self.checkAnsThree.enabled = YES;
    }
}

- (IBAction)qThreeAnsFour:(UIButton *)sender {
    
    self.ansFourCount++;
    
    if (self.ansFourCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsThree.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.answerOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.answerThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.checkAnsThree.enabled = YES;
    }
}
- (IBAction)qThreeResult:(UIButton *)sender {
    
    if (self.ansOneCount % 2 != 0) {
        
        [self.view setBackgroundColor:[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:0.7]];
        self.qThreeResultLabel.text = @"Correct! - Proceed to the Next Question.";
        self.answerOne.enabled = NO;
        self.answerTwo.enabled = NO;
        self.answerThree.enabled = NO;
        self.answerFour.enabled = NO;
        self.toQFour.enabled = YES;
        qThreeStore.numberOfCorrectAnswers++;
        qThreeStore.answerThree = TRUE;
    }
    
    else
    {
        [self.view setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7]];
        self.qThreeResultLabel.text = @"Incorrect! - Proceed to the Next Question.";
        self.answerOne.enabled = NO;
        self.answerTwo.enabled = NO;
        self.answerThree.enabled = NO;
        self.answerFour.enabled = NO;
        self.toQFour.enabled = YES;
        qThreeStore.answerThree = FALSE;
    }

    
    
}


@end
