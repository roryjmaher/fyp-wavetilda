//
//  quizQTwoVC.m
//  WaveTilda
//
//  Created by Rory Maher on 30/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "quizQTwoVC.h"

@interface quizQTwoVC ()
{
    Store *qTwoStore;
}

@end

@implementation quizQTwoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    qTwoStore = [Store sharedStore];
    self.ansOneCount = 0;
    self.ansTwoCount = 0;
    self.ansThreeCount = 0;
    self.ansFourCount = 0;
    
    self.checkAnsTwo.enabled = NO;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:37.0/255.0 green:53.0/255.0 blue:68.0/255.0 alpha:1.0]];
    self.toQuestionThree.enabled = NO;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.checkAnsTwo.enabled = NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)qTwoAnsOne:(UIButton *)sender
{
    
    self.ansOneCount++;
    
    if (self.ansOneCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsTwo.enabled = NO;
        
   
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.qTwoAnsTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsTwo.enabled = YES;
    }
}


- (IBAction)qTwoAnsTwo:(UIButton *)sender {
    
    self.ansTwoCount++;
    
    if (self.ansTwoCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsTwo.enabled = NO;
        
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.qTwoAnsOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsTwo.enabled = YES;
    }
}

- (IBAction)qTwoAnsThree:(UIButton *)sender {
    
    self.ansThreeCount++;
    
    if (self.ansThreeCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsTwo.enabled = NO;
        
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.qTwoAnsOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansFourCount = 0;
        self.checkAnsTwo.enabled = YES;
    }
}


- (IBAction)qTwoAnsFour:(UIButton *)sender {
    
    self.ansFourCount++;
    
    if (self.ansFourCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsTwo.enabled = NO;
        
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.qTwoAnsOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.qTwoAnsThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.checkAnsTwo.enabled = YES;
    }
    
}

- (IBAction)questionTwoResult:(UIButton *)sender {
    
    
    if (self.ansTwoCount % 2 != 0) {
        
        [self.view setBackgroundColor:[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:0.7]];
        self.questionTwoLabelResult.text = @"Correct! - Proceed to the Next Question.";
        self.qTwoAnsOne.enabled = NO;
        self.qTwoAnsTwo.enabled = NO;
        self.qTwoAnsThree.enabled = NO;
        self.qTwoAnsFour.enabled = NO;
        self.toQuestionThree.enabled = YES;
        qTwoStore.numberOfCorrectAnswers++;
        qTwoStore.answerTwo = TRUE;
    }
    
    else
    {
        [self.view setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7]];
        self.questionTwoLabelResult.text = @"Incorrect! - Proceed to the Next Question.";
        self.qTwoAnsOne.enabled = NO;
        self.qTwoAnsTwo.enabled = NO;
        self.qTwoAnsThree.enabled = NO;
        self.qTwoAnsFour.enabled = NO;
        self.toQuestionThree.enabled = YES;
        qTwoStore.answerTwo = FALSE;
        
    }
    
    
}
@end
