//
//  WaveTildaAppDelegate.h
//  WaveTilda
//
//  Created by Rory Maher on 05/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PdAudioController.h"
#import "PdDispatcher.h"
#import "PdBase.h"

@interface WaveTildaAppDelegate : UIResponder <UIApplicationDelegate, PdReceiverDelegate>
{
    PdAudioController *audioController;
    PdDispatcher *dispatcher;
        
}

@property (strong, nonatomic) UIWindow *window;




@end
