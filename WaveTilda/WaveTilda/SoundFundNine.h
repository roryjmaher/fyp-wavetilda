//
//  SoundFundNine.h
//  WaveTilda
//
//  Created by Rory Maher on 27/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoundFundNine : UIViewController
{
    void* patch;
}

- (IBAction)popToEight:(UIButton *)sender;
- (IBAction)menuFromNine:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextView *timbreTextOne;
@property (weak, nonatomic) IBOutlet UITextView *timbreTextTwo;

@property (weak, nonatomic) IBOutlet UIImageView *timbreImage;

@property (weak, nonatomic) IBOutlet UILabel *timbreLabel;

@property (nonatomic, assign) NSUInteger timbreCounter;

@property (weak, nonatomic) IBOutlet UIProgressView *timbreProgress;

- (IBAction)timbreTap:(UITapGestureRecognizer *)sender;

@end
