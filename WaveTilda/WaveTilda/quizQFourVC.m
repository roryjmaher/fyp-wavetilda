//
//  quizQFourVC.m
//  WaveTilda
//
//  Created by Rory Maher on 31/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "quizQFourVC.h"

@interface quizQFourVC ()
{
    Store *qFourStore;
}

@end

@implementation quizQFourVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    qFourStore = [Store sharedStore];
    self.ansOneCount = 0;
    self.ansTwoCount = 0;
    self.ansThreeCount = 0;
    self.ansFourCount = 0;
    
    self.checkAnsFour.enabled = NO;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:37.0/255.0 green:53.0/255.0 blue:68.0/255.0 alpha:1.0]];
    self.toQFiveButton.enabled = NO;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ansBtnOne:(UIButton *)sender {
    
    self.ansOneCount++;
    
    if (self.ansOneCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsFour.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansAttTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsFour.enabled = YES;
    }
}


- (IBAction)ansBtnTwo:(UIButton *)sender {
    
    self.ansTwoCount++;
    
    if (self.ansTwoCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsFour.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansAttOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansThreeCount = 0;
        self.ansFourCount = 0;
        self.checkAnsFour.enabled = YES;
    }
}

- (IBAction)ansBtnThree:(UIButton *)sender {
    
    self.ansThreeCount++;
    
    if (self.ansThreeCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsFour.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansAttOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttFour setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansFourCount = 0;
        self.checkAnsFour.enabled = YES;
    }
    
}

- (IBAction)ansBtnFour:(UIButton *)sender {
    
    self.ansFourCount++;
    
    if (self.ansFourCount % 2 == 0) {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsFour.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansAttOne setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttTwo setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansAttThree setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.ansOneCount = 0;
        self.ansTwoCount = 0;
        self.ansThreeCount = 0;
        self.checkAnsFour.enabled = YES;
    }

}
- (IBAction)questionFourResult:(UIButton *)sender {
    
    if (self.ansThreeCount % 2 != 0)
    {
        
        [self.view setBackgroundColor:[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:0.7]];
        self.qFourAnsLabel.text = @"Correct! - Proceed to the Next Question.";
        self.ansAttOne.enabled = NO;
        self.ansAttTwo.enabled = NO;
        self.ansAttThree.enabled = NO;
        self.ansAttFour.enabled = NO;
        self.toQFiveButton.enabled = YES;
        qFourStore.numberOfCorrectAnswers++;
        qFourStore.answerFour = TRUE;
    }
    
    else
    {
        [self.view setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7]];
        self.qFourAnsLabel.text = @"Incorrect! - Proceed to the Next Question.";
        self.ansAttOne.enabled = NO;
        self.ansAttTwo.enabled = NO;
        self.ansAttThree.enabled = NO;
        self.ansAttFour.enabled = NO;
        self.toQFiveButton.enabled = YES;
        qFourStore.answerFour = FALSE;
    }
}
@end
