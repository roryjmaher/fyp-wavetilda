//
//  quizQuestionOneVC.m
//  WaveTilda
//
//  Created by Rory Maher on 29/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "quizQuestionOneVC.h"

@interface quizQuestionOneVC ()
{
    Store *qOneStore;
}

@end

@implementation quizQuestionOneVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    qOneStore = [Store sharedStore];
    qOneStore.numberOfCorrectAnswers = 0;
    
    self.answerOneCount = 0;
    self.answerTwoCount = 0;
    self.answerThreeCount = 0;
    self.answerFourCount = 0;
    
    [self.view setBackgroundColor:[UIColor colorWithRed:37.0/255.0 green:53.0/255.0 blue:68.0/255.0 alpha:1.0]];
    self.toSecondQOutlet.enabled = NO;
    self.checkAnsOne.enabled = NO;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    self.answerOneCount = 0;
    self.answerTwoCount = 0;
    self.answerThreeCount = 0;
    self.answerFourCount = 0;
    self.checkAnsOne.enabled = NO;
    
    if(qOneStore.quizHelpOnce == FALSE)
    {
        [self performSegueWithIdentifier:@"quizHelpOnce" sender:self];
    }
    else
    {
        qOneStore.quizHelpOnce = TRUE;
        
    }
    qOneStore.quizHelpOnce = TRUE;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)qOneToMenu:(UIButton *)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)ansOneButton:(UIButton *)sender {
    
    self.answerOneCount++;
    
    if (self.answerOneCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsOne.enabled = NO;
        
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansTwoButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.answerTwoCount = 0;
        self.answerThreeCount = 0;
        self.answerFourCount = 0;
        self.checkAnsOne.enabled = YES;
        
    }
    
}

- (IBAction)ansTwoButton:(UIButton *)sender {
    
    self.answerTwoCount++;
    
    if (self.answerTwoCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsOne.enabled = NO;
        
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.answerOneCount = 0;
        self.answerThreeCount = 0;
        self.answerFourCount = 0;
        self.checkAnsOne.enabled = YES;
    }
    
}

- (IBAction)ansThreeButton:(UIButton *)sender {
    
    self.answerThreeCount++;
    
    if (self.answerThreeCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsOne.enabled = NO;
        
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansTwoButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansFourButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.answerOneCount = 0;
        self.answerTwoCount = 0;
        self.answerFourCount = 0;
        self.checkAnsOne.enabled = YES;
    }

}

- (IBAction)ansFourButton:(UIButton *)sender {
    
    self.answerFourCount++;
    
    if (self.answerFourCount % 2 == 0)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.checkAnsOne.enabled = NO;
    }
    else
    {
        [sender setBackgroundColor:[UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0]];
        [self.ansOneButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansThreeButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        [self.ansTwoButton setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0]];
        self.answerOneCount = 0;
        self.answerThreeCount = 0;
        self.answerTwoCount = 0;
        self.checkAnsOne.enabled = YES;
    }

}
//Checks answer disables buttons, changes view colors
- (IBAction)checkQOneAnswer:(UIButton *)sender {
    
    if (self.answerFourCount % 2 != 0) {
        [self.view setBackgroundColor:[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:0.7]];
        self.qOneResultLabel.text = @"Correct! - Proceed to the Next Question.";
        self.ansOneButton.enabled = NO;
        self.ansTwoButton.enabled = NO;
        self.ansThreeButton.enabled = NO;
        self.ansFourButton.enabled = NO;
        self.toSecondQOutlet.enabled = YES;
        qOneStore.numberOfCorrectAnswers++;
        qOneStore.answerOne = TRUE;
        
    }
    else
    {
        [self.view setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7]];
        self.qOneResultLabel.text = @"Incorrect! - Proceed to the Next Question.";
        self.ansOneButton.enabled = NO;
        self.ansTwoButton.enabled = NO;
        self.ansThreeButton.enabled = NO;
        self.ansFourButton.enabled = NO;
        self.toSecondQOutlet.enabled = YES;
        qOneStore.answerOne = FALSE;
    }
}
@end
