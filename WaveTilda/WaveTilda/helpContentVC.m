//
//  helpContentVC.m
//  WaveTilda
//
//  Created by Rory Maher on 02/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "helpContentVC.h"

@interface helpContentVC ()

@end

@implementation helpContentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.counter = 0;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)dismissContentHelp:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)menuButton:(UIButton *)sender {
    
    self.helpContentInteractionLabel.text = @"To Menu";
}

- (IBAction)nextButton:(UIButton *)sender {
    
    self.helpContentInteractionLabel.text = @"Next";
}

- (IBAction)previousButton:(UIButton *)sender {
    
    self.helpContentInteractionLabel.text = @"Previous";
}

- (IBAction)tapImage:(UITapGestureRecognizer *)sender {
    
    self.counter++;
    
    if (self.counter % 2 == 0) {
        self.helpContentInteractionLabel.text = @"";
    }
    else
    {
        self.helpContentInteractionLabel.text = @"Boo!";
        
    }
}

- (IBAction)slider:(UISlider *)sender {
    
    self.helpContentInteractionLabel.text = [NSString stringWithFormat:@"%0.1f",sender.value];
}
@end
