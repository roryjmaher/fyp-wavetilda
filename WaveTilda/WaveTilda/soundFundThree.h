//
//  soundFundThree.h
//  WaveTilda
//
//  Created by Rory Maher on 11/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PdDispatcher.h"
#import "PdBase.h"
#import "WaveTildaAppDelegate.h"

@interface soundFundThree : UIViewController
{
    void *patch;
}


@property (weak, nonatomic) IBOutlet UIImageView *speakerView;
@property (weak, nonatomic) IBOutlet UIImageView *propImage;


@property (nonatomic, assign) NSInteger speakerCounter;
@property (nonatomic, assign) NSInteger soundCounter;

- (IBAction)speakerStartStop:(UITapGestureRecognizer *)sender;
- (IBAction)propStartStop:(UITapGestureRecognizer *)sender;

- (IBAction)toTwo:(UIButton *)sender;
- (IBAction)threeToMenu:(UIButton *)sender;

@end
