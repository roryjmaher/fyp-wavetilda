//
//  quizRewards.h
//  WaveTilda
//
//  Created by Rory Maher on 01/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"
@interface quizRewards : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *rewardImageView;

@property (weak, nonatomic) IBOutlet UITextView *rewardMessage;



@end
