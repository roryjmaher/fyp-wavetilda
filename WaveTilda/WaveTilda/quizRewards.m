//
//  quizRewards.m
//  WaveTilda
//
//  Created by Rory Maher on 01/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "quizRewards.h"

@interface quizRewards ()
{
    Store *getCorrectQuestions;
    int localScore;
}

@end

@implementation quizRewards

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    getCorrectQuestions = [Store sharedStore];
    localScore = getCorrectQuestions.numberOfCorrectAnswers;
    
    [self userEarned];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)userEarned
{
    if (getCorrectQuestions.numberOfCorrectAnswers == 8)
    {
        self.rewardImageView.image = [UIImage imageNamed:@"PlatinumTrophy.png"];
        
        self.rewardMessage.text =  [NSString stringWithFormat:@"You Scored : %d - Perfect Score!\n You earned a Platinum Trophy!\n Well Done and Congratulations!!",localScore];
            getCorrectQuestions.trophyEarned = @"Platinum Trophy";
    }
    else if (getCorrectQuestions.numberOfCorrectAnswers == 7)
    {
        self.rewardImageView.image = [UIImage imageNamed:@"GoldTrophy.jpg"];
        self.rewardMessage.text = [NSString stringWithFormat:@"You Scored : %d - Almost Perfect!\n You earned a Gold Trophy!\n Well Done!!",localScore];
        getCorrectQuestions.trophyEarned = @"Gold Trophy";
    }
    else if (getCorrectQuestions.numberOfCorrectAnswers == 6)
    {
        self.rewardImageView.image = [UIImage imageNamed:@"SilverTrophy.jpg"];
        self.rewardMessage.text = [NSString stringWithFormat:@"You Scored : %d - Great Attempt!\n You earned a Silver Trophy\n Your getting there!",localScore];
        getCorrectQuestions.trophyEarned = @"Silver Trophy";
    }
    else if (getCorrectQuestions.numberOfCorrectAnswers == 5)
    {
        self.rewardImageView.image = [UIImage imageNamed:@"BronzeTrophy.jpg"];
        self.rewardMessage.text = [NSString stringWithFormat:@"You Scored : %d - Good Attempt!\n You earned a Bronze Trophy\nWell Done!!",localScore];
        getCorrectQuestions.trophyEarned = @"Bronze Trophy";
    }
    else if (getCorrectQuestions.numberOfCorrectAnswers > 1 && getCorrectQuestions.numberOfCorrectAnswers < 5)
    {
        self.rewardImageView.image = [UIImage imageNamed:@"KeepTrying.png"];
        self.rewardMessage.text = [NSString stringWithFormat:@"You Scored : %d - Not the best.\nYou need to revise the material again!",localScore];
        getCorrectQuestions.trophyEarned = @"Keep Trying Badge";
    }
    else
    {
        self.rewardImageView.image = [UIImage imageNamed:@"failureImage.jpg"];
        self.rewardMessage.text = [NSString stringWithFormat:@"You Scored : %d - Fail.\n Ok you can only get better from here.\nStart Again!",localScore];
        getCorrectQuestions.trophyEarned = @"Failure.";
    }

    
}

@end
