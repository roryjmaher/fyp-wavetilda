//
//  SoundFundEight.m
//  WaveTilda
//
//  Created by Rory Maher on 25/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "SoundFundEight.h"
#import "PdBase.h"
#import "PdDispatcher.h"
#import "WaveTildaAppDelegate.h"

@interface SoundFundEight ()
{
    NSMutableArray *phaseGifOneArray;
    NSMutableArray *phaseGifTwoArray;
}

@end

@implementation SoundFundEight

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [PdBase sendBangToReceiver:@"phaseVolUp"];
    
    patch = [PdBase openFile:@"phaseDemonstration.pd" path:[[NSBundle mainBundle] bundlePath]];
    self.phaseImageCounter = 0;
    self.phaseGifOneCounter = 0;
    self.phaseGifTwoCounter = 0;
    self.phaseText.text = @"Another characteristic of audio waveforms is Phase. This relates to the difference in time between two similar waveforms. \n\nIf we are recording a piece of audio and it is not in Phase then we generally consider it to be unwelcome. The unit of measurement for Phase is measured in degrees (e.g. 180°).";

    
    phaseGifOneArray = [NSMutableArray array];
    phaseGifTwoArray = [NSMutableArray array];
    
    for (NSUInteger i = 0; i < 65; i++) {
        NSString *phaseGifImgNames = [NSString stringWithFormat:@"PhaseShift_%lu.gif",(unsigned long)i];
        [phaseGifOneArray addObject:[UIImage imageNamed:phaseGifImgNames]];
    }
    self.phaseGifOne.animationImages = phaseGifOneArray;
    self.phaseGifOne.animationDuration = 4.0;
    
    
    for (NSUInteger j = 0; j < 13; j++) {
        NSString *phaseNamesTwo = [NSString stringWithFormat:@"phaseA_%lu.gif",(unsigned long)j];
        [phaseGifTwoArray addObject:[UIImage imageNamed:phaseNamesTwo]];
    }
    self.phaseGifTwo.animationImages = phaseGifTwoArray;
    self.phaseGifTwo.animationDuration = 12.0;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [PdBase sendBangToReceiver:@"phaseVolUp"];
    [PdBase sendBangToReceiver:@"phaseStop"];
    self.phaseImageCounter = 0;
    self.phaseGifOneCounter = 0;
    self.phaseGifTwoCounter = 0;
    self.phaseText.text = @"Another characteristic of audio waveforms is Phase. This relates to the difference in time between two similar waveforms. \n\nIf we are recording a piece of audio and it is not in Phase then we generally consider it to be unwelcome. The unit of measurement for Phase is measured in degrees (e.g. 180°).";
}

-(void)viewDidAppear:(BOOL)animated
{
    [PdBase sendBangToReceiver:@"phaseVolUp"];
    [PdBase sendBangToReceiver:@"phaseStop"];
    [self.phaseGifOne stopAnimating];
    [self.phaseGifTwo stopAnimating];
    self.phaseImageCounter = 0;
    self.phaseGifOneCounter = 0;
    self.phaseGifTwoCounter = 0;
    self.phaseImageProgess.progress = 0.25;
    self.phaseText.text = @"Another characteristic of audio waveforms is Phase. This relates to the difference in time between two similar waveforms. \n\nIf we are recording a piece of audio and it is not in Phase then we generally consider it to be unwelcome. The unit of measurement for Phase is measured in degrees (e.g. 180°).";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [PdBase sendBangToReceiver:@"phaseVolDown"];
    //[PdBase setDelegate:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [PdBase sendBangToReceiver:@"phaseVolDown"];
    //[PdBase setDelegate:nil];
}
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    [PdBase sendBangToReceiver:@"phaseVolDown"];
    //[PdBase setDelegate:nil];
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)popToSeven:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menuFromEight:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)phaseImageTap:(UITapGestureRecognizer *)sender
{
    [PdBase sendBangToReceiver:@"phaseStop"];
    [self.phaseGifOne stopAnimating];
    [self.phaseGifTwo stopAnimating];
    
    self.phaseImageCounter++;
    if (self.phaseImageCounter > 3) {
        self.phaseImageCounter = 0;
    }
    NSLog(@"Tap Number is: %lu",(unsigned long)self.phaseImageCounter);
    
    if (self.phaseImageCounter == 0)
    {
        self.phaseImageProgess.progress = 0.25;
        self.phaseImages.image = [UIImage imageNamed:@"Phase-Diagram.gif"];
        self.phaseText.text = @"Another characteristic of audio waveforms is Phase. This relates to the difference in time between two similar waveforms. \n\nIf we are recording a piece of audio and it is not in Phase then we generally consider it to be unwelcome. The unit of measurement for Phase is measured in degrees (e.g. 180°).";
    }
    
    else if (self.phaseImageCounter == 1)
    {
                self.phaseImageProgess.progress = 0.50;
        self.phaseImages.image = [UIImage imageNamed:@"wave-Phase.gif"];
        self.phaseText.text = @"Phase is often unwelcome because it can cause certain frequencies to be reduced or eliminated. This can cause audio to sound 'thin' or lacking in tone. Phase can also be used creatively in audio effects and compositions. \n\nThere are two main types of phase: Constructive and Destructive Phase. ";
    }
    
    else if (self.phaseImageCounter == 2)
    {
                self.phaseImageProgess.progress = 0.75;
        self.phaseImages.image = [UIImage imageNamed:@"phase_in.gif"];
        self.phaseText.text = @"Constructive Phase involves the Peaks of a waveform being perfectly in line with each other. When this happens our resulting audio is 'In Phase'. This is constructive as the waverforms interact together to reinforce each other. \n\nThe result is an audio output with an increased Amplitude or Voulme.";
    }
    
    else
    {
                self.phaseImageProgess.progress = 1;
        self.phaseImages.image = [UIImage imageNamed:@"phase_out.gif"];
                self.phaseText.text = @"Destructive Phase occurs when the peaks of the waveform are not perfectly aligned. This can cause the resulting sound to be thin and weedy. \n\nThis reduces and can cancel frequencies altogether. If Sine Waves are 180° out of phase then there would be no resulting sound as the waveforms cancel each other out.";
        
    }
    
    
}

- (IBAction)phaseGifOne:(UITapGestureRecognizer *)sender {
    
    self.phaseGifOneCounter++;
    
    if (self.phaseGifOneCounter % 2 == 0) {
        [self.phaseGifOne stopAnimating];
        [PdBase sendBangToReceiver:@"phaseStop"];
    }
    else
    {
        [self.phaseGifOne startAnimating];
        [self.phaseGifTwo stopAnimating];
        self.phaseGifTwoCounter = 0;
        [PdBase sendBangToReceiver:@"phaseStart"];
        self.phaseText.text = @"You will now hear seperate audio clips playing.\n\n1 = Original Audio File.\n2 = Audio Files In Phase.\n3 = Audio files Out of Phase.\nBelow, we can see an illustration of the effect of two waveforms moving in and out of Phase. ";
    }
}


- (IBAction)phaseGifTwo:(UITapGestureRecognizer *)sender {
    
    self.phaseText.text = @"In the bottom right corner we can see an animation. One waveform is being shifted in time from the left and then from the right in relation to another waveform. \n\nWe are measuring these movements in Pi (π) which is easier to express here rather than degrees. When the waveform is π (180°) out of Phase no sound will be produced.";
    
    [PdBase sendBangToReceiver:@"phaseStop"];
    self.phaseGifTwoCounter++;
   
    if (self.phaseGifTwoCounter % 2 == 0) {
        [self.phaseGifTwo stopAnimating];
    }
    else
    {
        [self.phaseGifTwo startAnimating];
        [self.phaseGifOne stopAnimating];
        self.phaseGifOneCounter = 0;
    }
}


@end
