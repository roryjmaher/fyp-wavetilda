//
//  drawAmplitudeView.h
//  WaveTilda
//
//  Created by Rory Maher on 21/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoundFundSix.h"

@class SoundFundSix;
@interface drawAmplitudeView : UIView

@property (assign, nonatomic) float amplitude;

@end
