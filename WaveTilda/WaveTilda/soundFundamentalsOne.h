//
//  soundFundamentalsOne.h
//  WaveTilda
//
//  Created by Rory Maher on 05/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PdDispatcher.h"
#import "PdBase.h"
#import "WaveTildaAppDelegate.h"

@interface soundFundamentalsOne : UIViewController
{
    void *patch;
}



@property (weak, nonatomic) IBOutlet UIImageView *alarmClockView;
@property (weak, nonatomic) IBOutlet UIImageView *talkingView;
@property (weak, nonatomic) IBOutlet UIImageView *arnoldView;


@property (nonatomic, assign) NSInteger alarmCounter;
@property (nonatomic, assign) NSInteger talkingCounter;
@property (nonatomic, assign) NSInteger arnoldCounter;

- (IBAction)alarmStartStop:(UITapGestureRecognizer *)sender;
- (IBAction)talkingStartStop:(UITapGestureRecognizer *)sender;
- (IBAction)arnoldStartStop:(UITapGestureRecognizer *)sender;


- (IBAction)menuOne:(UIButton *)sender;







@end
