//
//  quizQTwoVC.h
//  WaveTilda
//
//  Created by Rory Maher on 30/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"

@interface quizQTwoVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *checkAnsTwo;


//Butoons with answers
- (IBAction)qTwoAnsOne:(UIButton *)sender;
- (IBAction)qTwoAnsTwo:(UIButton *)sender;
- (IBAction)qTwoAnsThree:(UIButton *)sender;
- (IBAction)qTwoAnsFour:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *qTwoAnsOne;
@property (weak, nonatomic) IBOutlet UIButton *qTwoAnsTwo;
@property (weak, nonatomic) IBOutlet UIButton *qTwoAnsThree;
@property (weak, nonatomic) IBOutlet UIButton *qTwoAnsFour;

//- (IBAction)qTwoToMenu:(UIButton *)sender;

//- (IBAction)toQuestionThree:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *toQuestionThree;

@property (nonatomic, assign) NSUInteger ansOneCount;
@property (nonatomic, assign) NSUInteger ansTwoCount;
@property (nonatomic, assign) NSUInteger ansThreeCount;
@property (nonatomic, assign) NSUInteger ansFourCount;


- (IBAction)questionTwoResult:(UIButton *)sender;
//@property (weak, nonatomic) IBOutlet UIButton *questionTwoResult;

@property (weak, nonatomic) IBOutlet UILabel *questionTwoLabelResult;


//Questions Correct or Incorrect

//Current Number of Correct Answers

@end
