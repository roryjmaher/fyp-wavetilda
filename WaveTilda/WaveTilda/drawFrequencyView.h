//
//  drawFrequencyView.h
//  WaveTilda
//
//  Created by Rory Maher on 24/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoundFundSeven.h"

@class SoundFundSeven;
@interface drawFrequencyView : UIView

@property (nonatomic,assign) float frequency;

@end
