//
//  SoundFundSeven.h
//  WaveTilda
//
//  Created by Rory Maher on 24/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "drawFrequencyView.h"
#import "PdDispatcher.h"
#import "PdBase.h"
#import "WaveTildaAppDelegate.h"

@class drawFrequencyView;
@interface SoundFundSeven : UIViewController
{
    void *patch;
}

@property (weak, nonatomic) IBOutlet drawFrequencyView *freqView;

- (IBAction)freqSlider:(UISlider *)sender;
- (IBAction)popToSix:(UIButton *)sender;
- (IBAction)menuFromSeven:(UIButton *)sender;

- (IBAction)frequencyPinch:(UIPinchGestureRecognizer *)sender;
- (IBAction)frequencyTap:(UITapGestureRecognizer *)sender;

@property (weak, nonatomic) IBOutlet UIProgressView *frequencyProgress;
@property (nonatomic, assign) NSUInteger frequencyCounter;

@property (weak, nonatomic) IBOutlet UITextView *frequencyTextOne;
@property (weak, nonatomic) IBOutlet UITextView *frequencyTextTwo;

@property (weak, nonatomic) IBOutlet UIImageView *freqImages;


@end
