//
//  helpQuizVC.h
//  WaveTilda
//
//  Created by Rory Maher on 02/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface helpQuizVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *answerOneToggle;

- (IBAction)checkAnswer:(UIButton *)sender;
- (IBAction)toMenu:(UIButton *)sender;
- (IBAction)nextVC:(UIButton *)sender;


- (IBAction)answerOneButton:(UIButton *)sender;



- (IBAction)dismissTheHelp:(UIButton *)sender;


@end
