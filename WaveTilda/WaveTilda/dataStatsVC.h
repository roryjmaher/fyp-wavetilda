//
//  dataStatsVC.h
//  WaveTilda
//
//  Created by Rory Maher on 06/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "Store.h"


@interface dataStatsVC : UIViewController <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lastScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastPercentageLabel;


@property (weak, nonatomic) IBOutlet UILabel *answerOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerTwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerThreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerFourLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerFiveLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerSixLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerSevenLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerEightLabel;


@property (weak, nonatomic) IBOutlet UILabel *lastTrophyEarned;

- (IBAction)emailResults:(UIButton *)sender;
- (IBAction)dataToMenu:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *l1;
@property (weak, nonatomic) IBOutlet UILabel *l2;
@property (weak, nonatomic) IBOutlet UILabel *l3;
@property (weak, nonatomic) IBOutlet UILabel *l4;


@end
