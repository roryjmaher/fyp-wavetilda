//
//  soundFundTwo.m
//  WaveTilda
//
//  Created by Rory Maher on 10/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "soundFundTwo.h"
#import "PdBase.h"

@interface soundFundTwo ()

@end

@implementation soundFundTwo
{
    //I-Vars
    float rippleValue;
    CATransition *animation;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    // open one instance of the load-meter patch and forget about it
	patch = [PdBase openFile:@"ripple.pd" path:[[NSBundle mainBundle] bundlePath]];
    rippleValue = 3.5f;

	// Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated
{
    [PdBase setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


//Detects a touch using a tap recogniser and animates the image ripple
- (IBAction)rippleTouch:(UITapGestureRecognizer *)sender {
    
     animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:rippleValue];
    [animation setTimingFunction: [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setType:@"rippleEffect" ];
    [self.rippleImage.layer addAnimation:animation forKey:NULL];
    [PdBase sendBangToReceiver:@"rippleBang"];
}

- (IBAction)woodTap:(UITapGestureRecognizer *)sender {
    [PdBase sendBangToReceiver:@"woodBang"];
}

- (IBAction)metalTap:(UITapGestureRecognizer *)sender {
    [PdBase sendBangToReceiver:@"metalBang"];
}

- (IBAction)waterTap:(UITapGestureRecognizer *)sender {
    [PdBase sendBangToReceiver:@"waterBang"];
}




- (IBAction)menuTwo:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)popToOne:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
