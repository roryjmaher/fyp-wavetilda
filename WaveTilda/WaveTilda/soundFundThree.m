//
//  soundFundThree.m
//  WaveTilda
//
//  Created by Rory Maher on 11/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "soundFundThree.h"
#import "PdBase.h"
#import "PdDispatcher.h"
#import "WaveTildaAppDelegate.h"


@interface soundFundThree ()

@end

@implementation soundFundThree

{
    // I-Vars
    float speakerDuration;
    NSMutableArray *speakerArray;
    NSMutableArray *soundWave;
    NSMutableArray *driver;
    float sliderValue;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    patch = [PdBase openFile:@"speakerPropTone.pd" path:[[NSBundle mainBundle] bundlePath]];
	// Do any additional setup after loading the view.
    
    speakerDuration = 0.5f;
    self.speakerCounter = 0;
    self.soundCounter = 0;
    
    speakerArray = [NSMutableArray array];
    soundWave = [NSMutableArray array];
    driver = [NSMutableArray array];
    
    for (NSUInteger i = 1; i < 29; i++)
    {
        NSString *speakerImages = [NSString stringWithFormat:@"speaker_%lu.gif",(unsigned long)i];
        [speakerArray addObject:[UIImage imageNamed:speakerImages]];
    }
    self.speakerView.animationImages = speakerArray;
    self.speakerView.animationDuration = speakerDuration;
    
    for (NSUInteger j = 1; j < 11; j++)
    {
        NSString *soundImages = [NSString stringWithFormat:@"soundWave_%lu.gif",(unsigned long)j];
        [soundWave addObject:[UIImage imageNamed:soundImages]];
    }
    self.propImage.animationImages = soundWave;
    self.propImage.animationDuration = 0.4f;
    
    self.speakerCounter = 0;
    self.soundCounter = 0;
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    self.speakerCounter = 0;
    self.soundCounter = 0;
    [PdBase sendBangToReceiver:@"stopSpeaker"];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [PdBase sendBangToReceiver:@"stopSpeaker"];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.speakerView stopAnimating];
    [self.propImage stopAnimating];
    [PdBase sendBangToReceiver:@"stopSpeaker"];
    [PdBase setDelegate:nil];
    //[PdBase closeFile:patch];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)speakerStartStop:(UITapGestureRecognizer *)sender
{
    
    self.speakerCounter++;
    
    if (self.speakerCounter % 2 == 0)
    {
        [self.speakerView stopAnimating];
        [self.propImage stopAnimating];
        [PdBase sendBangToReceiver:@"stopSpeaker"];
    }
    else
    {
        [self.speakerView startAnimating];
        [self.propImage startAnimating];
        [PdBase sendBangToReceiver:@"startSpeaker"];
    }
    
    
}

- (IBAction)propStartStop:(UITapGestureRecognizer *)sender {
    
    self.soundCounter++;
    
    if (self.soundCounter % 2 == 0)
    {
        [self.propImage stopAnimating];
        [self.speakerView stopAnimating];
        [PdBase sendBangToReceiver:@"stopSpeaker"];

    }
    else
    {
        [self.speakerView startAnimating];
        [self.propImage startAnimating];
        [PdBase sendBangToReceiver:@"startSpeaker"];
    }
    
    
}


- (IBAction)toTwo:(UIButton *)sender {

    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)threeToMenu:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
