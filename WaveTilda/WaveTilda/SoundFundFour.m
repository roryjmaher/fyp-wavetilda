//
//  SoundFundFour.m
//  WaveTilda
//
//  Created by Rory Maher on 17/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "SoundFundFour.h"
#import "PdBase.h"
#import "PdDispatcher.h"
#import "WaveTildaAppDelegate.h"

@interface SoundFundFour ()

@end

@implementation SoundFundFour
{
    // I-Vars
    NSMutableArray *waveformArray;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    waveformArray = [NSMutableArray array];
    
    self.sineCounter = 0;
    self.wavelengthCounter = 0;
    self.amplitudeCounter = 0;
    self.frequencyCounter = 0;
    
    self.wavelengthLabel.text = @"";
    self.amplitudeLabel.text = @"";
    self.frequencyLabel.text = @"";
    
    for (NSUInteger i = 1; i < 51; i++)
    {
        NSString *waveformImages = [NSString stringWithFormat:@"waveform_%lu.gif",(unsigned long)i];
        [waveformArray addObject:[UIImage imageNamed:waveformImages]];
        
        self.waveformImageView.animationImages = waveformArray;
        self.waveformImageView.animationDuration = 1.2;
    }
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.sineCounter = 0;
    self.wavelengthLabel.text = @"";
    self.amplitudeLabel.text = @"";
    self.frequencyLabel.text = @"";
    self.wavelengthCounter = 0;
    self.amplitudeCounter = 0;
    self.frequencyCounter = 0;

}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.waveformImageView stopAnimating];

}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)soundFourMenuBtn:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    //Any action to be triggererd by clicking Main Menu
}


- (IBAction)popToThree:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sineStartStop:(UITapGestureRecognizer *)sender {
    
    self.sineCounter++;
    
    if (self.sineCounter % 2 == 0) {
        [self.waveformImageView stopAnimating];
    }
    else
    {
        [self.waveformImageView startAnimating];
        
    }
}

- (IBAction)wavelengthTap:(UITapGestureRecognizer *)sender {
    
    self.wavelengthCounter++;
    
    if (self.wavelengthCounter % 2 == 0) {
        self.wavelengthLabel.text = @"";
    }
    else
    {
        self.wavelengthLabel.text = @"Wavelength";
    }
}

- (IBAction)amplitudeTap:(UITapGestureRecognizer *)sender {
    
    self.amplitudeCounter++;
    
    if (self.amplitudeCounter % 2 == 0) {
        self.amplitudeLabel.text = @"";
    }
    else
    {
        self.amplitudeLabel.text = @"Amplitude";
    }
}

- (IBAction)frequencyTap:(UITapGestureRecognizer *)sender {
    
    self.frequencyCounter++;
    
    if (self.frequencyCounter % 2 == 0) {
        self.frequencyLabel.text = @"";
    }
    else
    {
        self.frequencyLabel.text = @"Frequency";
    }
}


@end
