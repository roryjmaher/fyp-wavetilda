//
//  feedBackVC.h
//  WaveTilda
//
//  Created by Rory Maher on 03/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "Store.h"

@interface feedBackVC : UIViewController <MFMailComposeViewControllerDelegate, UITextViewDelegate>

- (IBAction)dismissFeedback:(UIButton *)sender;



@property (weak, nonatomic) IBOutlet UITextView *understandAppText;
- (IBAction)understandAppYes:(UIButton *)sender;
- (IBAction)appUnderstandNo:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *understandYes;
@property (weak, nonatomic) IBOutlet UIButton *understandNo;


@property (weak, nonatomic) IBOutlet UITextView *easyToUse;
- (IBAction)easyToUseYes:(UIButton *)sender;
- (IBAction)easyToUseNo:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *understandAttYes;
@property (weak, nonatomic) IBOutlet UIButton *understandAttNo;



@property (weak, nonatomic) IBOutlet UITextView *contentUnderstand;
- (IBAction)contentYes:(UIButton *)sender;
- (IBAction)contentNo:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *contentAttYes;
@property (weak, nonatomic) IBOutlet UIButton *contentAttNo;



@property (weak, nonatomic) IBOutlet UITextView *appUseful;
- (IBAction)usefulYes:(UIButton *)sender;
- (IBAction)usefulNo:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *usefulAttYes;
@property (weak, nonatomic) IBOutlet UIButton *usefulAttNo;


- (IBAction)dismissKeyboard:(UITapGestureRecognizer *)sender;
- (IBAction)sendFeedback:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UILabel *l1;
@property (weak, nonatomic) IBOutlet UILabel *l2;
@property (weak, nonatomic) IBOutlet UILabel *l3;
@property (weak, nonatomic) IBOutlet UILabel *l4;

@property (nonatomic, copy) NSString *q1String;
@property (nonatomic, copy) NSString *q2String;
@property (nonatomic, copy) NSString *q3String;
@property (nonatomic, copy) NSString *q4String;




@end
