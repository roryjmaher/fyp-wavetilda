//
//  SoundFundSeven.m
//  WaveTilda
//
//  Created by Rory Maher on 24/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "SoundFundSeven.h"
#import "PdBase.h"
#import "PdDispatcher.h"
#import "WaveTildaAppDelegate.h"

@interface SoundFundSeven ()


@end

@implementation SoundFundSeven

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    patch = [PdBase openFile:@"TwentyTwentyFreqSweep.pd" path:[[NSBundle mainBundle] bundlePath]];
    self.freqView.frequency = 1;
    self.frequencyCounter = 0;
    self.frequencyTextOne.text = @"Frequency relates to our perception of 'Pitch' in audio and music. This is affected by the rate at which the source object vibrates.";
    self.frequencyTextTwo.text = @"The closer together the Peaks of the waveform are the higher the frequency. Also the further apart the Peaks are the lower the Frequency.";
    [PdBase sendBangToReceiver:@"sweepDownVol"];
    [PdBase sendBangToReceiver:@"setBottom"];
}

-(void)viewWillAppear:(BOOL)animated
{
    //[PdBase sendBangToReceiver:@"sweepDownVol"];
    //[PdBase sendBangToReceiver:@"setBottom"];
    self.freqView.frequency = 1;
    self.frequencyCounter = 0;
    self.frequencyTextOne.text = @"Frequency relates to our perception of 'Pitch' in audio and music. This is affected by the rate at which the source object vibrates.";
   self.frequencyTextTwo.text = @"The closer together the Peaks of the waveform are the higher the frequency. Also the further apart the Peaks are the lower the Frequency.";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [PdBase sendBangToReceiver:@"sweepDownVol"];
    [PdBase setDelegate:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [PdBase sendBangToReceiver:@"sweepDownVol"];
    [PdBase setDelegate:nil];
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}


- (IBAction)freqSlider:(UISlider *)sender {
    
    self.freqView.frequency = sender.value;
}

- (IBAction)popToSix:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menuFromSeven:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)frequencyPinch:(UIPinchGestureRecognizer *)sender {
    
    //ibooks IOS 7 App Development Essentials book for this code!
    float freqScale = [(UIPinchGestureRecognizer*)sender scale];
    
    freqScale = (freqScale * 1.5) + 1;
    
    if (freqScale > 80) {
        freqScale = 80;
    }
    
    self.freqView.frequency = freqScale;
}

- (IBAction)frequencyTap:(UITapGestureRecognizer *)sender {
    
    self.frequencyCounter++;
    
    if (self.frequencyCounter > 4)
    {
        self.frequencyCounter = 0;
    }
    
    if (self.frequencyCounter == 0) {
        
        [PdBase sendBangToReceiver:@"sweepDownVol"];
        self.freqImages.image = [UIImage imageNamed:@"frequency-01.png"];
        self.frequencyTextOne.text = @"Frequency relates to our perception of 'Pitch' in audio and music. This is affected by the rate at which the source object vibrates.";
        self.frequencyTextTwo.text = @"The closer together the Peaks of the waveform are - the higher the frequency. Also, the further apart the Peaks are - the lower the Frequency.";
        self.frequencyProgress.progress = 0.2;
        [PdBase sendBangToReceiver:@"setBottom"];

    }
    else if (self.frequencyCounter == 1)
    {
        [PdBase sendBangToReceiver:@"sweepDownVol"];
        self.freqImages.image = [UIImage imageNamed:@"Frequency-02.JPG"];
        self.frequencyTextOne.text = @"The lower the Frequency - the lower we percieve the pitch to be. The higher the frequency - the higher we perceive pitch.";
        self.frequencyTextTwo.text = @"We measure Frequency in Hertz (Hz).";
        self.frequencyProgress.progress = 0.4;
        [PdBase sendBangToReceiver:@"setBottom"];

    }
    else if (self.frequencyCounter == 2)
    {
        [PdBase sendBangToReceiver:@"sweepDownVol"];
        self.freqImages.image = [UIImage imageNamed:@"frequency-003.jpg"];
        self.frequencyTextOne.text = @"Another term used for Frequency is Cycles Per Second. This represents the amount of wavelengths that pass a specific point in time in one second.";
        self.frequencyTextTwo.text = @"Humans can hear sound within a specific range. This ranges from 20Hz to 20,000Hz (20kHz).";
        self.frequencyProgress.progress = 0.6;
        [PdBase sendBangToReceiver:@"setBottom"];
    }
    else if (self.frequencyCounter == 3)
    {
        self.freqImages.image = [UIImage imageNamed:@"Frequency-04.jpg"];
        self.frequencyTextOne.text = @"Here, we can hear the human frequency range playing, starting at 20Hz and finishing at 20kHz. As we age our ability to hear higher frequencies reduces.";
        self.frequencyTextTwo.text = @"Each animal has a unique range of frequency which they can hear. This is why dogs and cats can hear higher frequencies that we cannot.";
        self.frequencyProgress.progress = 0.8;
        [PdBase sendBangToReceiver:@"sweepUpVol"];
        [PdBase sendBangToReceiver:@"freqSweepUp"];

        
    }
    
    else
    {
        [PdBase sendBangToReceiver:@"sweepDownVol"];
        //[PdBase sendBangToReceiver:@"freqSweepDown"];
        self.freqImages.image = [UIImage imageNamed:@"fletcherMunson.jpg"];
        self.frequencyTextOne.text = @"We are designed to hear certain frequencies better than others. Frequencies between 2kHz and 5kHz are easiest to hear. This range is important to us as speech is in this range.";
        self.frequencyTextTwo.text = @"Lower frequencies require more intensity to be heard clearly. This Equal Loudness curve (left) shows how much intensity is required for us to percieve equal volume across all Frequencies.";
        self.frequencyProgress.progress = 1;

        
    }
}

    @end
