//
//  main.m
//  WaveTilda
//
//  Created by Rory Maher on 05/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WaveTildaAppDelegate.h"
#import "appOverviewVC.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WaveTildaAppDelegate class]));
    }
}
