//
//  quizQuestionOneVC.h
//  WaveTilda
//
//  Created by Rory Maher on 29/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"

@interface quizQuestionOneVC : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *checkAnsOne;


- (IBAction)qOneToMenu:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextView *questionOneTextField;

// Action Buttons
- (IBAction)ansOneButton:(UIButton *)sender;
- (IBAction)ansTwoButton:(UIButton *)sender;
- (IBAction)ansThreeButton:(UIButton *)sender;
- (IBAction)ansFourButton:(UIButton *)sender;

//Button Outlets
@property (weak, nonatomic) IBOutlet UIButton *ansOneButton;
@property (weak, nonatomic) IBOutlet UIButton *ansTwoButton;
@property (weak, nonatomic) IBOutlet UIButton *ansThreeButton;
@property (weak, nonatomic) IBOutlet UIButton *ansFourButton;
@property (weak, nonatomic) IBOutlet UIButton *toSecondQOutlet;


//Checks for Correct Answer
- (IBAction)checkQOneAnswer:(UIButton *)sender;

//Outputs Text whether the answer was correct or not
@property (weak, nonatomic) IBOutlet UILabel *qOneResultLabel;


//Counters for calcualting answer logic
@property (nonatomic, assign) NSUInteger answerOneCount;
@property (nonatomic, assign) NSUInteger answerTwoCount;
@property (nonatomic, assign) NSUInteger answerThreeCount;
@property (nonatomic, assign) NSUInteger answerFourCount;



@end
