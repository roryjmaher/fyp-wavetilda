//
//  SoundFundSix.h
//  WaveTilda
//
//  Created by Rory Maher on 20/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "drawAmplitudeView.h"

@class drawAmplitudeView;
@interface SoundFundSix : UIViewController




@property (weak, nonatomic) IBOutlet drawAmplitudeView *ampView;

@property (nonatomic, assign) NSUInteger amplitudeCounter;

@property (weak, nonatomic) IBOutlet UIImageView *amplitudeImages;

- (IBAction)ampSlider:(UISlider *)sender;

- (IBAction)popToFive:(UIButton *)sender;
- (IBAction)toMenuSix:(UIButton *)sender;

- (IBAction)pinchAmplitude:(UIPinchGestureRecognizer *)sender;
- (IBAction)amplitudeTap:(UITapGestureRecognizer *)sender;

@property (weak, nonatomic) IBOutlet UITextView *amplitudeText;
@property (weak, nonatomic) IBOutlet UITextView *amplitudeTextTwo;

@property (weak, nonatomic) IBOutlet UIProgressView *amplitudeProgress;

@end
