//
//  WaveTildaViewController.h
//  WaveTilda
//
//  Created by Rory Maher on 05/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaveTildaViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *mainMenuLabel;
@property (strong, nonatomic) IBOutlet UIButton *soundFundChapterOne;
@property (strong, nonatomic) IBOutlet UILabel *menuDescriptionLbl;

@end
