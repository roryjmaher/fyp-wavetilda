//
//  quizQThreeVC.h
//  WaveTilda
//
//  Created by Rory Maher on 31/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"

@interface quizQThreeVC : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *checkAnsThree;


@property (weak, nonatomic) IBOutlet UIButton *toQFour;
@property (weak, nonatomic) IBOutlet UILabel *qThreeResultLabel;

//Button Presses
- (IBAction)qThreeAnsOne:(UIButton *)sender;
- (IBAction)qThreeAnsTwo:(UIButton *)sender;
- (IBAction)qThreeAnsThree:(UIButton *)sender;
- (IBAction)qThreeAnsFour:(UIButton *)sender;

//Button Attributes
@property (weak, nonatomic) IBOutlet UIButton *answerOne;
@property (weak, nonatomic) IBOutlet UIButton *answerTwo;
@property (weak, nonatomic) IBOutlet UIButton *answerThree;
@property (weak, nonatomic) IBOutlet UIButton *answerFour;


- (IBAction)qThreeResult:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *qThreeResultAttribute;

@property (nonatomic, assign) NSUInteger ansOneCount;
@property (nonatomic, assign) NSUInteger ansTwoCount;
@property (nonatomic, assign) NSUInteger ansThreeCount;
@property (nonatomic, assign) NSUInteger ansFourCount;

@end
