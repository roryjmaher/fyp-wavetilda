//
//  drawAmplitudeView.m
//  WaveTilda
//
//  Created by Rory Maher on 21/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "drawAmplitudeView.h"

@implementation drawAmplitudeView



- (void)setAmplitude:(float)value
{
	if (value != self.amplitude)
    {
		_amplitude = value;
		[self setNeedsDisplay];
	}
    
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}



// Only override drawRect: if you perform custom drawing.
// This is where I want to use Core graphics to manipulate a waveform either using a slider.
- (void)drawRect:(CGRect)rect  
{
    // Sine Wave Drawing code
    
    //Method Specific Variables
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    float halfHeight = height / 2;
    
    //These variables are the animatable properties (Specifically amplitude in this subclass).
    float frequency = 5;
    
    // Creates a drawing context
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 31.0/255.0f, 33.0/255.0f, 36.0/255.0f, 1.0f);
    CGContextFillRect(context, rect);
    
    //Creates an RGB Color for the Sine Wave itself
    [[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:1] set];
    
    //Sets thickness of the Line which draws the sine wave
    CGContextSetLineWidth(context, 8);
    
    //Creates curves at intersects
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
    //Mathaatical drawing for Sine Wave
    for(CGFloat x = 0; x < width; x += 0.5)
    {
        CGFloat y = self.amplitude * sinf(2 * M_PI * (x / width) * frequency) + halfHeight;
        
        if(x == 0)
            CGContextMoveToPoint(context, x, y);
        else
            CGContextAddLineToPoint(context, x, y);
    }
    
    CGContextStrokePath(context);
}


@end
