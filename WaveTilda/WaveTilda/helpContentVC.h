//
//  helpContentVC.h
//  WaveTilda
//
//  Created by Rory Maher on 02/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface helpContentVC : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *helpContentInteractionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)menuButton:(UIButton *)sender;
- (IBAction)nextButton:(UIButton *)sender;
- (IBAction)previousButton:(UIButton *)sender;
- (IBAction)tapImage:(UITapGestureRecognizer *)sender;
- (IBAction)slider:(UISlider *)sender;


@property (nonatomic, assign) int counter;

@end
