//
//  SoundFundEight.h
//  WaveTilda
//
//  Created by Rory Maher on 25/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoundFundEight : UIViewController
{
    void *patch;
}

- (IBAction)popToSeven:(UIButton *)sender;
- (IBAction)menuFromEight:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIPageControl *phaseProgress;

@property (weak, nonatomic) IBOutlet UIImageView *phaseImages;
@property (weak, nonatomic) IBOutlet UITextView *phaseText;

@property (nonatomic, assign) NSUInteger phaseImageCounter;

- (IBAction)phaseImageTap:(UITapGestureRecognizer *)sender;


@property (weak, nonatomic) IBOutlet UIImageView *phaseGifOne;
@property (weak, nonatomic) IBOutlet UIImageView *phaseGifTwo;


- (IBAction)phaseGifTwo:(UITapGestureRecognizer *)sender;
- (IBAction)phaseGifOne:(UITapGestureRecognizer *)sender;

@property (nonatomic, assign) NSUInteger phaseGifOneCounter;
@property (nonatomic, assign) NSUInteger phaseGifTwoCounter;


@property (weak, nonatomic) IBOutlet UIProgressView *phaseImageProgess;

@end
