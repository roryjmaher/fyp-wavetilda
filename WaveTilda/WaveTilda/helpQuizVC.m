//
//  helpQuizVC.m
//  WaveTilda
//
//  Created by Rory Maher on 02/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "helpQuizVC.h"

@interface helpQuizVC ()
{
    int count;
}

@end

@implementation helpQuizVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    count = 0;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)checkAnswer:(UIButton *)sender {
}

- (IBAction)toMenu:(UIButton *)sender {
}

- (IBAction)nextVC:(UIButton *)sender {
}

- (IBAction)answerOneButton:(UIButton *)sender {
    
    count++;
    
    if (count %2 == 0) {
        self.answerOneToggle.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
    }
    else
    {
        self.answerOneToggle.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
    }
    
}

- (IBAction)dismissTheHelp:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
