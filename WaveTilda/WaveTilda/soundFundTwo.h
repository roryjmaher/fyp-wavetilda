//
//  soundFundTwo.h
//  WaveTilda
//
//  Created by Rory Maher on 10/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PdDispatcher.h"
#import "PdBase.h"
#import "WaveTildaAppDelegate.h"

@interface soundFundTwo : UIViewController
{
    void *patch;
}


- (IBAction)popToOne:(UIButton *)sender;



@property (weak, nonatomic) IBOutlet UIImageView *rippleImage;

- (IBAction)rippleTouch:(UITapGestureRecognizer *)sender;
- (IBAction)woodTap:(UITapGestureRecognizer *)sender;
- (IBAction)metalTap:(UITapGestureRecognizer *)sender;
- (IBAction)waterTap:(UITapGestureRecognizer *)sender;





- (IBAction)menuTwo:(UIButton *)sender;

@end
