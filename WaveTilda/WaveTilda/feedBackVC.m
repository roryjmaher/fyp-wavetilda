//
//  feedBackVC.m
//  WaveTilda
//
//  Created by Rory Maher on 03/04/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "feedBackVC.h"

@interface feedBackVC ()
{
    
    Store *feedBackStore;
    
    int countB1;
    int countB2;
    int countB3;
    int countB4;
    int countB5;
    int countB6;
    int countB7;
    int countB8;
    
    NSString *q1Text;
    NSString *q2Text;
    NSString *q3Text;
    NSString *q4Text;
   
    NSString *emailContent;

}

@end

@implementation feedBackVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    feedBackStore = [Store sharedStore];
    
    countB1 = 0;
    countB2 = 0;
    countB3 = 0;
    countB4 = 0;
    countB5 = 0;
    countB6 = 0;
    countB7 = 0;
    countB8 = 0;
    
    self.q1String = [[NSString alloc]init];
    self.q2String = [[NSString alloc]init];
    self.q3String = [[NSString alloc]init];
    self.q4String = [[NSString alloc]init];
    

    
    //emailContent = [NSString stringWithFormat:@" %@ ",self.q1String];
    
}

-(void)viewDidAppear:(BOOL)animated
{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismissFeedback:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


// ------ Better Understanding ------
- (IBAction)understandAppYes:(UIButton *)sender {

    countB1++;
    
    if (countB1 % 2 == 0)
    {
        self.understandYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
    }
    else
    {
        self.understandYes.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.understandNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB2 = 0;
        feedBackStore.feedBackOneBool = @"Yes";
        q1Text = self.understandAppText.text;
    }
}


- (IBAction)appUnderstandNo:(UIButton *)sender {
    
    countB2++;
    
    if (countB2 % 2 == 0)
    {
        self.understandNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
    }
    else
    {
        self.understandNo.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.understandYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB1 = 0;
        feedBackStore.feedBackOneBool = @"No";
        q1Text = self.understandAppText.text;
    }

}


// ------ App Easy ------
- (IBAction)easyToUseYes:(UIButton *)sender {
    
    countB3++;
    
    if (countB3 % 2 == 0)
    {
        self.understandAttYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        
    }
    else
    {
        self.understandAttYes.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.understandAttNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB4 = 0;
        feedBackStore.feedBackTwoBool = @"Yes";
        q2Text = self.easyToUse.text;
    }
}

- (IBAction)easyToUseNo:(UIButton *)sender {
    
    countB4++;
    
    if (countB4 % 2 == 0)
    {
        self.understandAttNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        
    }
    else
    {
        self.understandAttNo.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.understandAttYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB3 = 0;
        feedBackStore.feedBackTwoBool = @"No";
        q2Text = self.easyToUse.text;
    }
}

//-----------------------------------



// ------ Content Easy ------------------

- (IBAction)contentYes:(UIButton *)sender {
    
    countB5++;
    
    if (countB5 % 2 == 0)
    {
        self.contentAttYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        
    }
    else
    {
        self.contentAttYes.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.contentAttNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB6 = 0;
        feedBackStore.feedBackThreeBool = @"Yes";
    }
}

- (IBAction)contentNo:(UIButton *)sender {
    
    countB6++;
    
    if (countB6 % 2 == 0)
    {
        self.contentAttNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        
    }
    else
    {
        self.contentAttNo.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.contentAttYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB5 = 0;
        feedBackStore.feedBackThreeBool = @"No";
    }

}

//-----------------------------------

// ------ App Useful --------------
- (IBAction)usefulYes:(UIButton *)sender {
    
    countB7++;
    
    if (countB7 % 2 == 0)
    {
        self.usefulAttYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        
    }
    else
    {
        self.usefulAttYes.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.usefulAttNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB8 = 0;
        feedBackStore.feedBackFourBool = @"Yes";
    }

    
}

- (IBAction)usefulNo:(UIButton *)sender {
    
    countB8++;
    
    if (countB8 % 2 == 0) {
        self.usefulAttNo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
    }
    else
    {
        self.usefulAttNo.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:158.0/255.0 blue:233.0/255.0 alpha:1.0];
        self.usefulAttYes.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:76.0/255.0 blue:1.0/255.0 alpha:1.0];
        countB7 = 0;
        feedBackStore.feedBackFourBool = @"No";
    }
}

//-----------------------------------


- (IBAction)dismissKeyboard:(UITapGestureRecognizer *)sender {
    
    [self.understandAppText resignFirstResponder];
    [self.easyToUse resignFirstResponder];
    [self.contentUnderstand resignFirstResponder];
    [self.appUseful resignFirstResponder];
    
}

- (IBAction)sendFeedback:(UIButton *)sender {
    
    q1Text = self.understandAppText.text;
    q2Text = self.easyToUse.text;
    q3Text = self.contentUnderstand.text;
    q4Text = self.appUseful.text;
    
    
    emailContent = [NSString stringWithFormat:@" %@: %@ \nComment if Any: %@\n\n\n%@: %@ \nComment if Any: %@\n\n\n%@: %@ \nComment if Any: %@\n\n\n%@: %@ \nComment if Any: %@\n\n\n",
                    self.l1.text,
                    feedBackStore.feedBackOneBool,
                    q1Text,
                    self.l2.text,
                    feedBackStore.feedBackTwoBool,
                    q2Text,
                    self.l3.text,
                    feedBackStore.feedBackThreeBool,
                    q3Text,
                    self.l4.text,
                    feedBackStore.feedBackFourBool,
                    q4Text];
    
    MFMailComposeViewController *feedbackController = [[MFMailComposeViewController alloc] init];
    [feedbackController setMailComposeDelegate:self];
    NSString *email;
    email = @"10132708@studentmail.ul.ie";
    NSArray *emailArrays = [[NSArray alloc] initWithObjects:email, nil];
    [feedbackController setToRecipients:emailArrays];
    [feedbackController setSubject:@"FYP - User Feedback"];
    [feedbackController setMessageBody:emailContent isHTML:NO];
    [self presentViewController:feedbackController animated:YES completion:nil];
    
}



-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
