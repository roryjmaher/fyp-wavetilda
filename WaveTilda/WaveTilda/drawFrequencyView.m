//
//  drawFrequencyView.m
//  WaveTilda
//
//  Created by Rory Maher on 24/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "drawFrequencyView.h"

@implementation drawFrequencyView




- (void)setFrequency:(float)value
{
	if (value != self.frequency)
    {
		_frequency = value;
		[self setNeedsDisplay];
	}
    
}





- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}




// Only override drawRect: if you perform custom drawing.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    float halfHeight = height / 2;
    
    const float amplitude = 100;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 31.0/255.0f, 33.0/255.0f, 36.0/255.0f, 1.0f);
    CGContextFillRect(context, rect);
    
    //Creates an RGB Color for the Sine Wave itself
    [[UIColor colorWithRed:73.0/255.0 green:162.0/255.0 blue:68.0/255.0 alpha:1] set];
    
    //Sets thickness of the Line which draws the sine wave
    CGContextSetLineWidth(context, 8);
    
    //Creates curves at intersects
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
    //Mathamatical drawing for Sine Wave
    for(CGFloat x = 0; x < width; x += 0.5)
    {
        CGFloat y = amplitude * sinf(2 * M_PI * (x / width) * self.frequency) + halfHeight;
        
        if(x == 0)
            CGContextMoveToPoint(context, x, y);
        else
            CGContextAddLineToPoint(context, x, y);
    }
    
    CGContextStrokePath(context);

}
    
    



@end
