//
//  WaveTildaViewController.m
//  WaveTilda
//
//  Created by Rory Maher on 05/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "WaveTildaViewController.h"
#import "appOverviewVC.h"
#import "Store.h"

@interface WaveTildaViewController ()
{
    Store *objectiveLoaded;
}

@end

@implementation WaveTildaViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    objectiveLoaded = [Store sharedStore];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (objectiveLoaded.objectivesOnce == TRUE) {
        [self performSegueWithIdentifier:@"overviewSegue" sender:self];
    }
    else
    {
        objectiveLoaded.objectivesOnce = FALSE;
    }
    objectiveLoaded.objectivesOnce = FALSE;
    //[self performSegueWithIdentifier:@"overviewSegue" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


@end
