//
//  dataStatsVC.m
//  WaveTilda
//
//  Created by Rory Maher on 06/02/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import "dataStatsVC.h"
#import "Store.h"

@interface dataStatsVC ()
{
    Store *displayStats;
    NSString *correctAnswer;
    NSString *inCorrectAnswer;
    float percentage;
    int totalScoreSession;
    int currentScore;
    
    NSString *emailContent;
}

@end

@implementation dataStatsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    correctAnswer = @"Correct";
    inCorrectAnswer = @"Incorrect";
    displayStats = [Store sharedStore];
    
    
    self.lastScoreLabel.text = [NSString stringWithFormat:@"Last Score was %d out of 8",displayStats.numberOfCorrectAnswers];
    
    [self setIdividualQuestionResults];
    [self calculatePercentage];
    [self recentTrophyEarned];
    
    emailContent = [NSString stringWithFormat:@"%@                  %@\n%@         %@\n\n%@                  \n\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n\n%@                  %@",
                    self.l1.text,
                    self.lastScoreLabel.text,
                    self.l2.text,
                    self.lastPercentageLabel.text,
                    self.l3.text,
                    self.answerOneLabel.text,
                    self.answerTwoLabel.text,
                    self.answerThreeLabel.text,
                    self.answerFourLabel.text,
                    self.answerFiveLabel.text,
                    self.answerSixLabel.text,
                    self.answerSevenLabel.text,
                    self.answerEightLabel.text,
                    self.l4.text,
                    self.lastTrophyEarned.text];
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)emailResults:(UIButton *)sender {
    
    
    MFMailComposeViewController *mailcontroller = [[MFMailComposeViewController alloc] init];
    [mailcontroller setMailComposeDelegate:self];
    NSString *email;
    email = @"10132708@studentmail.ul.ie";
    NSArray *emailArray = [[NSArray alloc] initWithObjects:email, nil];
    [mailcontroller setToRecipients:emailArray];
    [mailcontroller setSubject:@"FYP - User Quiz Results"];
    [mailcontroller setMessageBody:emailContent isHTML:NO];
    [self presentViewController:mailcontroller animated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)dataToMenu:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)setIdividualQuestionResults
{
    //Question 1
    if (displayStats.answerOne == TRUE) {
        self.answerOneLabel.text = [NSString stringWithFormat:@"Question 1 was -- %@",correctAnswer];
    }
    else
    {
        self.answerOneLabel.text = [NSString stringWithFormat:@"Question 1 was -- %@",inCorrectAnswer];
    }
    
    //Question 2
    if (displayStats.answerTwo == TRUE) {
        self.answerTwoLabel.text = [NSString stringWithFormat:@"Question 2 was -- %@",correctAnswer];
    }
    else
    {
        self.answerTwoLabel.text = [NSString stringWithFormat:@"Question 2 was -- %@",inCorrectAnswer];
    }
    
    //Question 3
    if (displayStats.answerThree == TRUE) {
        self.answerThreeLabel.text = [NSString stringWithFormat:@"Question 3 was -- %@",correctAnswer];
    }
    else
    {
        self.answerThreeLabel.text = [NSString stringWithFormat:@"Question 3 was -- %@",inCorrectAnswer];
    }
    
    //Question 4
    if (displayStats.answerFour == TRUE) {
        self.answerFourLabel.text = [NSString stringWithFormat:@"Question 4 was -- %@",correctAnswer];
    }
    else
    {
        self.answerFourLabel.text = [NSString stringWithFormat:@"Question 4 was -- %@",inCorrectAnswer];
    }
    
    //Question 5
    if (displayStats.answerFive == TRUE) {
        self.answerFiveLabel.text = [NSString stringWithFormat:@"Question 5 was -- %@",correctAnswer];
    }
    else
    {
        self.answerFiveLabel.text = [NSString stringWithFormat:@"Question 5 was -- %@",inCorrectAnswer];
    }
    
    //Question 6
    if (displayStats.answerSix == TRUE) {
        self.answerSixLabel.text = [NSString stringWithFormat:@"Question 6 was -- %@",correctAnswer];
    }
    else
    {
        self.answerSixLabel.text = [NSString stringWithFormat:@"Question 6 was -- %@",inCorrectAnswer];
    }
    
    //Question 7
    if (displayStats.answerSeven == TRUE) {
        self.answerSevenLabel.text = [NSString stringWithFormat:@"Question 7 was -- %@",correctAnswer];
    }
    else
    {
        self.answerSevenLabel.text = [NSString stringWithFormat:@"Question 7 was -- %@",inCorrectAnswer];
    }
    
    
    //Question 8
    if (displayStats.answerEight == TRUE) {
        self.answerEightLabel.text = [NSString stringWithFormat:@"Question 8 was -- %@",correctAnswer];
    }
    else
    {
        self.answerEightLabel.text = [NSString stringWithFormat:@"Question 8 was -- %@",inCorrectAnswer];
    }
    
}

-(void)calculatePercentage
{
    percentage = ((float)displayStats.numberOfCorrectAnswers / (float) 8.0) * 100;
    self.lastPercentageLabel.text = [NSString stringWithFormat:@"Last Percentage was %0.1f%%",percentage];
}

-(void)recentTrophyEarned
{
    self.lastTrophyEarned.text = [NSString stringWithFormat:@"Last Trophy Was - %@",displayStats.trophyEarned];
    
}




@end
