//
//  quizQSixVC.h
//  WaveTilda
//
//  Created by Rory Maher on 31/03/2014.
//  Copyright (c) 2014 com.maher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"

@interface quizQSixVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *toQSeven;
@property (weak, nonatomic) IBOutlet UILabel *qSixResultLabel;

@property (weak, nonatomic) IBOutlet UIButton *checkAnsSix;


- (IBAction)ansOne:(UIButton *)sender;
- (IBAction)ansTwo:(UIButton *)sender;
- (IBAction)ansThree:(UIButton *)sender;
- (IBAction)ansFour:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UIButton *ansOneAtt;
@property (weak, nonatomic) IBOutlet UIButton *ansTwoAtt;
@property (weak, nonatomic) IBOutlet UIButton *ansThreeAtt;
@property (weak, nonatomic) IBOutlet UIButton *ansFourAtt;

@property (nonatomic, assign) NSUInteger ansOneCount;
@property (nonatomic, assign) NSUInteger ansTwoCount;
@property (nonatomic, assign) NSUInteger ansThreeCount;
@property (nonatomic, assign) NSUInteger ansFourCount;

- (IBAction)qSixResult:(UIButton *)sender;

@end
